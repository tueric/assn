import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class cs341a3
{
  static int L[][] ;
  static int len;
  static String str;
  
  private static int LPS()
  {
    len = str.length();
    int i, j, x;
    L = new int[len][len]; 
    for (i = 0; i < len; i++) 
    {
      L[i][i] = 1;
    }
    for (x=2; x<=len; x++)
    {
      for (i=0; i<len-x+1; i++)
      {
        j = i+x-1;
        if (str.charAt(i) == str.charAt(j) && x == 2){
          L[i][j] = 2;
        }
        else if (str.charAt(i) == str.charAt(j)){
          L[i][j] = L[i+1][j-1] + 2;
        }
        else{
          L[i][j] = Math.max(L[i][j-1], L[i+1][j]);
        }
      }
    }
    return L[0][len-1];
  }
  
  private static String traceBack() {
    int lpslen = L[0][len-1];
    char result [] = new char[lpslen +1];
    int i = 0;
    int j = len-1;
    while( lpslen >= 0 && i<= j)
    {
      if(str.charAt(i)==str.charAt(j))
      {
        lpslen --;
        result[lpslen] = str.charAt(i);
        i++;
        j--;
      }
      else
      {
        if(L[i+1][j]>L[i][j-1])
        {
          i++;
        }
        else
        {
          j--;
        }
      }
    }
    int e = L[0][len-1]%2 ;
    i = 0;
    int mpoint = L[0][len-1]/2;
    j= result.length -2;
    if(e==0)        
    {  
      while(j>=mpoint)
      {
        result[i]= result[j];
        i++;
        j--;
      }
    }
    else
    {
      while(j>mpoint)
      {
        
        result[i]= result[j];
        i++;
        j--;
      }
    }
    String ret = new String(result);
    
    return ret;
  }
  
  public static void main(String args[])
  {
    BufferedReader b = new BufferedReader(new InputStreamReader(System.in)); 
    try{
      str = b.readLine();
    }
    catch (IOException e){
      e.printStackTrace();
    }
    int n = str.length();
    
    System.out.println(LPS());
    System.out.println(traceBack());
  }
}

