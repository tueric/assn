#include <iostream>
#include <string>

using namespace std;

void invalid (string *symbols,  string state, string symb){
    for(int i = 0 ; i < 80 ; i++){
        size_t found = symb.find(symbols[i]);
        if (found == std::string::npos){
            cout << state << " " << symbols[i] << " " << "invalid" << endl;
        }
    }
}
//not allowed number to start with zero
int main() {
    string states [27] = {"start", "invalid", "zero", "id", "lparen", "rparen", "lbrace", "rbrace", "becomes", "eq","exclem", "ne" ,"lt", "gt", "le", "ge", "plus","minus", "star", "slash", "pct", "comma", "semi", "lbrack","rbrack", "amp", "num" };
    string symb[80] = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z", "(",")","{","}","=","!","<",">","+","-","*","/", "%", ",",";","[","]","&"};

    cout << 80 << endl;
    for(int i = 0 ; i < 80 ; i++)
        cout << symb[i] << endl;

    cout << 27 << endl;
    for(int i = 0 ; i < 27 ; i++)
        cout << states[i] <<endl;
    cout << "start" << endl;
    cout << "24" << endl;
    for(int i = 0 ; i < 27 ; i++){
        if(states[i] != "invalid" && states[i] != "exclem" && states[i] != "start")
            cout << states[i] << endl;
    }
    cout << 2143 << endl;
    //insert print num transitions
    cout << "start " << "0" << " zero" << endl;
    invalid (symb, "zero", "");
    for(int i = 1; i < 10 ; i++){
        cout << "start " << symb[i] << " num" << endl;
    }
    for(int i = 10 ; i < 36+26; i++){
        cout << "start " << symb[i] << " id" << endl;
    }
//////////////
    cout << "start " << "(" << " lparen" <<endl;
    invalid(symb, "lparen", "(");
    cout << "start " << ")" << " rparen" << endl;
    invalid(symb, "rparen", ")");
    cout << "start " << "{" << " lbrace" << endl;
    invalid(symb, "lbrace", "{");
    cout << "start " << "}" << " rbrace" << endl;
    invalid(symb, "rbrace", "}");
    cout << "start " << "=" << " becomes" << endl; //remove the eq and excelm
    invalid(symb, "becomes", "==");
    cout << "start " << "!" << " exclem" << endl; //remove eq
    invalid(symb, "exclem", "!=");
    cout << "exclem " << "=" << " ne" << endl;
    invalid(symb, "ne", "");
    cout << "becomes " << "=" << " eq" << endl;
    invalid(symb, "eq", "" );
    cout << "start " << "<" << " lt" << endl; //remove eq
    invalid(symb, "lt", "<=");
    cout << "start " << ">" << " gt" << endl; //remove eq
    invalid(symb, "gt", ">=");
    cout << "lt " << "=" << " le" << endl;
    invalid(symb, "le", "");
    cout << "gt " << "=" << " ge" << endl;
    invalid(symb, "ge", "");
    cout << "start  " << "+" << " plus" << endl;
    invalid(symb, "plus", "+");
    cout << "start  " << "-" << " minus" << endl;
    invalid(symb, "minus", "-");
    cout << "start  " << "*" << " star" << endl;
    invalid(symb, "star", "*");
    cout << "start  " << "/" << " slash" << endl;
    invalid(symb, "slash", "/");
    cout << "start " << "%" << " pct" << endl;
    invalid(symb, "pct", "%");
    cout << "start " << "," << " comma" << endl;
    invalid(symb, "comma", ",");
    cout << "start " << ";" << " semi" << endl;
    invalid(symb, "semi", ";");
    cout << "start " << "[" << " lbrack" << endl;
    invalid(symb, "lbrack", "[");
    cout << "start " << "]" << " rbrack" << endl;
    invalid(symb, "rbrack", "]");
    cout << "start " << "&" << " amp" << endl;
    invalid (symb, "amp", "&");
////////////////
    for(int i = 0 ; i < 10 ; i++){
        cout << "num " << symb[i] << " num"<<endl;
    }
    for(int i = 10; i < 80 ; i++){
        cout << "num " << symb[i] << " invalid"<<endl;
    }
    for(int i = 0; i < 10+26+26 ; i++){
        cout << "id " << symb[i] << " id"<<endl;
    }
    for(int i = 10+26+26; i < 80; i++){
        cout << "id " <<symb[i] << " invalid"<<endl;
    }
    for(int i = 0; i < 80 ; i++){
        cout << "invalid " << symb[i]  << " invalid" <<endl;
    }
}
