; 1 = 49 in ascii

lis $10 
.word 10

lis $30 
.word 0xffff000c

lis $24
.word 48

lis $26 ;counter
.word 0

lis $14
.word 4

lis $11 
.word 1

lis $15
.word -4

lis $5
.word -1

lis $6
.word 45


slt $3, $1, $0
beq $0, $3, 3
mult $5, $1
mflo $1
sw $6, 0($30)

divu $1, $10
mfhi $29
mflo $1
add $25, $29, $24
mult $26, $14
mflo $27
sw $25, 0($27)
add $26, $26, $11
bne $0, $1, -9

lw $21, 0($27)
sw $21, 0($30)
sub $27, $27, $14
bne $15, $27, -4
sw $10, 0($30)
jr $31
