lw $3, 0($1) ;Load the first element
lis $11 ;store the number 1 for comparison
.word 1 
bne $11, $2, 1 ;if the length of array is 1, then we jp $31
jr $31 ;quit since array length is 1, were done

lis $20 ;currindex
.word 0 
lis $14 ;used for going to next element 
.word 4 

mult $14, $20
mflo $21
add $21, $1, $21
lw $4, 0($21)
slt $30, $3, $4
beq $30, $0, 1
add $3, $0, $4
add $20, $20, $11
bne $20, $2, -9
jr $31
