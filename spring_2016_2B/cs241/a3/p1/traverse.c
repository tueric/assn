#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

struct tree {
	int val;
	int num_children;
	struct tree **list_of_children;
};

void traverse(struct tree *root)
{
    if (root)
    {
        printf("%d %d\n", root->val, root->num_children);
        for (int i = 0; i < root->num_children; i++)
            traverse(root->list_of_children[i]);
    }
}

struct tree *newTree (int val, int num_children){
	struct tree *t = (struct tree*)malloc(sizeof(struct tree));
	t->val =val;
	t->num_children = num_children;
	t->list_of_children = malloc(num_children * sizeof(struct tree *));
	int i;
	for(i = 0 ; i < num_children ; i++){
		t->list_of_children[i] = NULL;
	}
	return t;
}

int *scan (struct tree &root){
	int val;
	int num_children;
	if (scanf ( "%d %d", &val, &num_children) == EOF){
		return 1;
	}
	root = newTree(val, num_children);
	int i;
	for(int i = 0; i < num_children; i++){
		if(scan(root->list_of_children[i]))
			break;
	}
	return 0;
}

// int deSerialize(Node *&root, FILE *fp)
// {
//     // Read next item from file. If theere are no more items or next
//     // item is marker, then return 1 to indicate same
//     char val;
//     if ( !fscanf(fp, "%c ", &val) || val == MARKER )
//        return 1;
 
//     // Else create node with this item and recur for children
//     root = newNode(val);
//     for (int i = 0; i < N; i++)
//       if (deSerialize(root->child[i], fp))
//          break;
 
//     // Finally return 0 for successful finish
//     return 0;
// }

struct tree *createDummyTree()
{
    struct tree *root = newTree(1,3);
    root->list_of_children[0] = newTree(2,2);
    root->list_of_children[1] = newTree(3,0);
    root->list_of_children[2] = newTree(4,4);

    root->list_of_children[0]->list_of_children[0] = newTree(5,0);
    root->list_of_children[0]->list_of_children[1] = newTree(6,1);
    root->list_of_children[2]->list_of_children[0] = newTree(7,0);
    root->list_of_children[2]->list_of_children[1] = newTree(8,0);
    root->list_of_children[2]->list_of_children[2] = newTree(9,0);
    root->list_of_children[2]->list_of_children[3] = newTree(10,0);

    root->list_of_children[0]->list_of_children[1]->list_of_children[0] = newTree(11,0);
    return root;
}
int main() {


	struct tree *t = NULL;
	scan(t);
	if(t == NULL){
		printf("IZ NULL\n");
	}
	traverse(t);
}
