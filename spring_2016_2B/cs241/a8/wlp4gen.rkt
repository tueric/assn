#lang racket

(define term (list "BOF" "EOF" "ID" "NUM" "LPAREN" "RPAREN" "LBRACE" "RBRACE" "RETURN" "IF" "ELSE" "WHILE" "PRINTLN" "WAIN" "BECOMES" "INT" "EQ" "NE" "LT" "GT" "LE" "GE" "PLUS" "MINUS" "STAR" "SLASH" "PCT" "COMMA" "SEMI" "NEW" "DELETE" "LBRACK" "RBRACK" "AMP" "NULL"))

(struct token (kind lexeme) #:transparent)
(struct p-node (value children) #:transparent)

(define symbol-table (make-hash))

(define (add-local-var func var)
  (define var-hash (hash-ref symbol-table func))
  (cond
    [(hash-has-key? var-hash (token-lexeme (p-node-value (second (p-node-children var)))))
     (error "ERROR variable alerady exists")]
    [else (hash-ref! var-hash
                     (token-lexeme (p-node-value (second (p-node-children var))))
                     (cond
                       [(= 2 (length (p-node-children (first (p-node-children var)))))
                        (string-append(token-lexeme(p-node-value (first (p-node-children (first (p-node-children var))))))
                          (token-lexeme (p-node-value (second (p-node-children (first(p-node-children var)))))))]
                       [else (token-lexeme (p-node-value (first (p-node-children (first (p-node-children var))))))]))]))

(define (id-define func n)
  (cond
    [(member (token-lexeme (p-node-value n)) term) void]
    [(equal? (token-kind (p-node-value n)) "ID")
     (define func-hash (hash-ref symbol-table func))
     (cond
       [(or
          (hash-has-key? symbol-table (token-lexeme(p-node-value n)))
          (hash-has-key? func-hash (token-lexeme(p-node-value n)))) void ]
       [else (error "ERROR not defined " n)])]

     [else
       (map (lambda (x) (id-define func x)) (p-node-children n))]))

(define (type-check func n)
    (define (lofstring-to-string lists)
      (string-append* (rest (append* (map (lambda (x) (list " " x))
                                                    lists)))))
  (cond [(empty? (p-node-children n)) ""]
        [(equal? (token-kind (p-node-value n)) "expr")
         (cond [(= 1 (length (p-node-children n))) (type-check func (first (p-node-children n)))]
               [else
                 (define first-thing (type-check func (first (p-node-children n))))
                 (define second-thing (type-check func (third (p-node-children n))))
                 (cond 
                 [(equal? "PLUS" (token-kind(p-node-value (second (p-node-children n)))))
                  (cond
                    [(string=? "int" first-thing second-thing) "int"]
                    [(or (and (equal? "int*" first-thing)
                              (equal? "int" second-thing))
                         (and (equal? "int" first-thing)
                              (equal? "int*" second-thing)))
                     "int*"]
                    [else (error "ERROR plus dun goofy")])]
                 [(equal? "MINUS" (token-kind (p-node-value (second (p-node-children n)))))
                  (cond
                    [(or (string=? "int*" first-thing second-thing) (string=? "int" first-thing second-thing)) "int"]
                    [(and (equal? "int*" first-thing)
                          (equal? "int" second-thing)) "int*"])])])]
        [(equal? (token-kind (p-node-value n)) "term")
         (cond [( = 3 (length (p-node-children n)))
                (define first-thing (type-check func (first (p-node-children n))))
                (define second-thing (type-check func (third (p-node-children n))))
                (cond [(string=? "int" first-thing second-thing) "int"]
                      [else (error "ERROR dun goofied on mult or div or mod")])]
               [else (type-check func (first (p-node-children n)))])]
        [(equal? (token-kind (p-node-value n)) "statement")
         (cond
           [(equal? "lvalue" (token-kind (p-node-value (first (p-node-children n)))))
            (cond [(not (equal? (type-check func (first (p-node-children n)))
                           (type-check func (third (p-node-children n)))))
                   (error "ERROR something something lvalue...")])]
           [(equal? "IF" (token-kind (p-node-value(first (p-node-children n)))))
            (cond [(not (equal? "type???" (type-check func (third (p-node-children n)))))
                  (error "ERROR dun gooffy on le IF "(type-check func (third (p-node-children n))))]
                  [else (type-check func (sixth (p-node-children n)))
                        (type-check func (tenth(p-node-children n)))])]
           [(equal? "WHILE" (token-kind (p-node-value(first (p-node-children n)))))
            (cond [(not(equal? "type???" (type-check func (third (p-node-children n)))))
                   (error "ERROR dun gooooooff on le WHILE")]
                  [else (type-check func (sixth (p-node-children n))) ])]
           [(equal? "DELETE" (token-kind (p-node-value(first (p-node-children n)))))
            (cond [(equal? "int*" (type-check func (fourth (p-node-children n)))) "int*"]
                  [else (error "ERROR DELETE IS NO GUDDDD")])]
           [(equal? "PRINTLN" (token-kind(p-node-value (first (p-node-children n)))))
            (cond [(equal? "int" (type-check func (third (p-node-children n)))) "int"]
                  [else (error "ERROR PRINTLN IS NO GOOOOOD HAHAHAH")])]
           [else (map (lambda (x) (type-check func x))(p-node-children n))])]
        [(equal? (token-kind (p-node-value n)) "factor")
         (cond
           [(= 1 (length (p-node-children n)))
            (cond [(equal? "ID" (token-kind(p-node-value(first (p-node-children n)))))
                   (cond [(hash-has-key? (hash-ref symbol-table func) (token-lexeme (p-node-value(first (p-node-children n)))))
                          (hash-ref (hash-ref symbol-table func) (token-lexeme (p-node-value(first (p-node-children n)))))]
                         [else (error "ERROR not found ~a " (token-lexeme (p-node-value(first (p-node-children n)))))])]
                  [(equal? "NUM" (token-kind(p-node-value(first (p-node-children n)))))
                   "int"]
                  [(equal? "NULL"(token-kind(p-node-value(first (p-node-children n)))))
                   "int*"]
                  [else (error "ERROR somthing wrong tieh id num null")])]
           [(= 2 (length (p-node-children n)))
            (cond [(equal? "AMP" (token-kind(p-node-value(first (p-node-children n)))))
                   (cond [(equal? (type-check func (second (p-node-children n))) "int") "int*"]
                         [else (error "ERROR AMP")])]
                  [(equal? "STAR" (token-kind(p-node-value (first (p-node-children n)))))
                   (define temp (type-check func (second (p-node-children n))))
                   (cond [(equal? temp "int*") "int"]
                         [else (error "ERROR STAR ~a " temp)])] 
                  [else (error "SHOULD NEVER HIT HERE LMAOOOO")])]
           [(= 3 (length (p-node-children n)))
            (cond [(equal? "LPAREN" (token-kind(p-node-value (first (p-node-children n)))))
                   (type-check func (second (p-node-children n)))]
                  [(equal? "ID" (token-kind(p-node-value(first (p-node-children n)))))
                   (cond [(and (hash-has-key? symbol-table (token-lexeme (p-node-value (first (p-node-children n)))))
                               (empty? (hash-ref symbol-table (token-lexeme (p-node-value (first (p-node-children n)))))))
                          "int"]
                         [else (error "ERROR FUN NOT DEFINED YO")])]
                  [else (error "SHOULD NEVER HIT HERE 2222")])]
           [(= 4 (length (p-node-children n)))
            (cond [(equal? "ID" (token-kind (p-node-value(first (p-node-children n)))))
                   (cond [(and (hash-has-key? symbol-table (token-lexeme (p-node-value (first (p-node-children n)))))
                               (equal? (cond [(hash-has-key? (hash-ref symbol-table (token-lexeme (p-node-value (first (p-node-children n))))) 123)
                                         (lofstring-to-string (hash-ref (hash-ref symbol-table (token-lexeme (p-node-value (first (p-node-children n))))) 123 ))]
                                             [else (error "ERROR cant find 123 or something")])
                                       (type-check func (third (p-node-children n))))) "int"]
                         [else (error "ERROR not defined y????oooo ~a " (hash-ref (hash-ref symbol-table (token-lexeme (p-node-value (first (p-node-children n))))) 123))])]
                  [else (error "ERROR uh oh dont think you shoudl be here")])]
           [(= 5 (length (p-node-children n)))
            (cond [(equal? "NEW" (token-kind (p-node-value (first (p-node-children n)))))
                   (cond [(equal? "int" (type-check func (fourth (p-node-children n))))
                          "int*"]
                         [else (error "ERROR in NEW")])]
                  [else (error "ERROR shouldnt bethere again sto pls")])]
           [else (error "ERROR factor")])]
        [(equal? (token-kind (p-node-value n)) "test")
         (cond
           [(equal? (type-check func (first (p-node-children n))) (type-check func(third (p-node-children n)))) "type???"]
           [else "notype"])]
        [(equal? (token-kind(p-node-value n)) "lvalue")
         (cond
           [(=  1 (length (p-node-children n)))
            (cond [(equal? "ID" (token-kind (p-node-value (first (p-node-children n)))))
                   ;;REMEMBER TO ERROR TRAP
                   (hash-ref (hash-ref symbol-table func) (token-lexeme (p-node-value(first (p-node-children n))))  )]
                  [else (error "ERROR should no be here again lval id")])]
           [(= 2 (length (p-node-children n)))
            (cond [(and (equal? "STAR" (token-kind (p-node-value (first (p-node-children n)))))
                        (equal? "int*" (type-check func (second (p-node-children n)))))
                   "int"])]
           [(= 3 (length (p-node-children n)))
            (cond [(equal? "LPAREN" (token-kind (p-node-value (first (p-node-children n)))))
                   (type-check func (second (p-node-children n)))])])]
        [(equal? "arglist" (token-kind (p-node-value n)))
         (cond [(= 1 (length (p-node-children n)))
                (type-check func (first (p-node-children n)))]
               [(= 3 (length (p-node-children n)))
                (type-check func (first (p-node-children n)))
                (type-check func (third (p-node-children n)))]
               [else (error "ERROR arglist err")])]
        [(equal? "dcls" (token-kind (p-node-value n)))
         (cond
           [(empty? (p-node-children n)) "good"]
           [(equal? (token-kind (p-node-value (fourth (p-node-children n)))) "NUM")
            (cond [(equal? (token-kind (p-node-value (first (p-node-children (first (p-node-children (second (p-node-children n)))))))) "INT")
                   (type-check func (first (p-node-children n)))]
                  [else (error "ERROR dcl")])]
           [(equal? (token-kind(p-node-value(fourth (p-node-children n)))) "NULL")
            
            (define pair (p-node-children (first  (p-node-children (second (p-node-children n))))))
            (cond [(and (= 2 (length pair))
                    (and (equal? (token-kind (p-node-value (first pair))) "INT")
                        (equal? (token-kind (p-node-value (second pair))) "STAR")))
                   (type-check func (first(p-node-children n)))]
                  [else (error "ERROR dcl2")])])]
        [else (map (lambda (x) (type-check func x)) (p-node-children n))]))



(define (passone n)
  (define (set-signature func var)
    (cond [(empty? (p-node-children var)) void]
          [(equal? (token-kind (p-node-value var)) "dcl")
           (define type
             (cond
               [(= 2 (length (p-node-children (first (p-node-children var)))))
                (string-append(token-lexeme(p-node-value (first (p-node-children (first (p-node-children var))))))
                  (token-lexeme (p-node-value (second (p-node-children (first(p-node-children var)))))))]
               [else (token-lexeme (p-node-value (first (p-node-children (first (p-node-children var))))))]))
           (add-local-var func var)
           (hash-set!
             (hash-ref symbol-table func) 123
             (cons type (hash-ref! (hash-ref symbol-table func) 123 empty )))]
          [else (map (lambda (x) (set-signature func x)) (p-node-children var))]))
  (define (dcls func var)
    (cond
      [(empty? (p-node-children var)) void]
      [(equal? (token-kind (p-node-value var)) "dcl")
       (add-local-var func var)]
      [else (map (lambda (x) (dcls func x)) (p-node-children var))]))
  (cond
    [(member (token-kind (p-node-value n)) term) void]
    [(equal? (token-kind (p-node-value n)) "main")
     (hash-ref! symbol-table "wain" (make-hash))
     (set-signature "wain" (fourth (p-node-children n)))
     (set-signature "wain" (sixth (p-node-children n)))
     (dcls "wain" (ninth (p-node-children n)))
     (id-define "wain" (tenth (p-node-children n)))
     (id-define "wain" (tenth (rest (rest (p-node-children n)))))
     (type-check "wain" (tenth (p-node-children n)))
     (type-check "wain" (ninth (p-node-children n)))
     (type-check "wain" (tenth (rest (rest (p-node-children n)))))
     (cond [(not(equal? "int" (second  (hash-ref (hash-ref symbol-table "wain") 123))))
            (error "ERROR wrong param types for wain")])
     (cond [(not (equal? "int" (type-check "wain" (tenth (rest (rest (p-node-children n)))))))
            (error "ERROR return type is wrong for wain" (type-check "wain" (tenth (rest (rest (p-node-children n))))))])]
    [(equal? (token-kind (p-node-value n)) "procedure")
        (define has-key? (hash-has-key? symbol-table (token-lexeme (p-node-value (second (p-node-children n))))))
        (cond
          [(not has-key?)
           (define funcname (token-lexeme (p-node-value(second (p-node-children n)))))
           (hash-ref! symbol-table funcname (make-hash))
           (set-signature funcname (fourth (p-node-children n)))
           (dcls funcname (seventh (p-node-children n)))
           (id-define funcname (eighth (p-node-children n)))
           (id-define funcname (tenth (p-node-children n)))
           (type-check funcname (eighth (p-node-children n)))
           (type-check funcname (tenth (p-node-children n)))
           (type-check funcname (seventh (p-node-children n)))]
          [else (error "ERROR function defined")])]
    [else (map (lambda (x) (passone x)) (p-node-children n))]))



(define (read-whole-program)
  (define line (read-line))
  (cond
    [(eof-object? line) empty]
    [else (cons (string-split line) (read-whole-program))]))

(define (make-tree lofprodrules t)
  (cond
    [(empty? lofprodrules) (first t)]
    [(member (first (first lofprodrules)) term)
     (make-tree
       (rest lofprodrules)
       (cons (p-node (token (first (first lofprodrules)) (second (first lofprodrules))) empty) t))]
    [else (make-tree
            (rest lofprodrules)
            (cons (p-node (token (first (first lofprodrules))
                                 (first (first lofprodrules)))
                          (take t  (- (length (first lofprodrules)) 1)))
                  (list-tail t (- (length (first lofprodrules)) 1))))]))

(define (main)
  (passone (make-tree (reverse (read-whole-program)) empty))
  (hash-for-each symbol-table
                 (lambda (k v)
                   (cond [(not (equal? "wain" k) )
                          (eprintf "~a" k)
                          (cond
                            [(not (empty? (hash-ref! (hash-ref symbol-table k) 123 empty)))
                             (define str (string-append* (rest (append* (map (lambda (x) (list " " x))
                                                                             (reverse (hash-ref! (hash-ref symbol-table k) 123 empty)))))))
                             (cond
                               [(equal? "" str) void]
                               [else (eprintf " ~a~n" str)])]
                            [else (eprintf "~n")])
                          (hash-for-each v
                                         (lambda (kay vee)
                                           (cond
                                             [(not (equal? 123 kay))(eprintf "~a ~a~n" kay vee)]
                                             [else void])))
                          (eprintf "~n")])))

                 (cond [(not (empty? (hash-ref! (hash-ref symbol-table "wain") 123  empty)))
                        (define str (string-append* (rest (append* (map (lambda (x) (list " " x))
                                                                        (reverse (hash-ref! (hash-ref symbol-table "wain") 123 empty)))))))
                        (eprintf "wain")
                        (cond
                          [(equal? "" str) void]
                          [else (eprintf " ~a~n" str)])])

                 (hash-for-each (hash-ref symbol-table "wain")
                                (lambda (kay vee)
                                  (cond
                                    [(not (equal? 123 kay))(eprintf "~a ~a~n" kay vee)]
                                    [else void]))))

;(passone (make-tree (reverse (read-whole-program))empty))
;(printf "~a~n" symbol-table)
(main)
