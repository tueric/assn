#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

void count_sort(int *arr, int k, int size){
    int c [k];
    for(int i = 0 ; i < k ; i++){
        c[i] = 0;
    }
    for(int i = 0 ; i < size; i++){
        c[arr[i]]++;
    }
    int l [k] = {0};
    for(int i = 1;  i < k ; i++){
        l[i] = l[i-1] + c[i-1];
    }
    std::vector<int> b(size);
    copy(arr, arr+size, b.begin());
    for(int i = 0 ; i < size ;i ++){
        arr[l[b[i]]] = b[i];
        l[b[i]]++;
    }
}


int main(){
    int size;
    cin >> size;
    int *list;
    list = new int[size];
    int max;
    int min;
    cin >> list[0];
    max = list[0];
    min = list[0];
    for(int i = 1 ; i < size ; i++){
        cin >> list[i];
        if(list[i] > max)
            max = list[i];
        if(list[i] < min)
            min = list[i];
    }

    for(int i = 0 ; i < size ; i++){
        list[i] = list[i] - min;
    }
    int k = max - min+1;
    count_sort (list,k,size);
    for(int i = 0 ; i < size ; i ++){
        cout << list[i] + min <<endl;
    }
    delete list;
}

