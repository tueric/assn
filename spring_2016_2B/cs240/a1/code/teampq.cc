#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Team {
    int wins;
    int losses;
    string name;
    public:
        Team(int wins, int losses, string name){
            this->name = name;
            this->wins = wins;
            this->losses = losses;
        }
        ~Team(){
        }
        int getWins(){
            return wins;
        }
        int getLosses(){
            return losses;
        }

        string getName(){
            return name;
        }
};

class TeamPQ {
    vector <&Team> pq;
    // add fields/methods/constructors/destructor as necessary
    public:
        void insert(const Team &t);   // O(log n) time
        const Team &findMaxWins() const;    // O(1) time
        const Team &findMinLosses() const;  // O(1) time
        void removeMaxWins();     // O(log n) time
        void removeMinLosses();   // O(log n) time
};


int main(int argc, char *argv[]) {
  cout << "Number of arguments: " << argc - 1 << endl;
  cout << "Arguments: " << endl;

  //Note that loop starts from index 1
  for (int i = 1; i < argc; ++i) {
    string theArg = argv[i];
    cout << " " << i << ": " << theArg << endl;
  }
}
